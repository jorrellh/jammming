import React from 'react';
import './TrackList.css';
import Track from '../Track/Track';

class TrackList extends React.Component {
    render() {
        return (
            <div className="TrackList">
                {this.props.tracks.map(track => <Track artist={track.artist} album={track.album} name={track.name} key={track.id}/>)}
            </div>
        )
    }
}

export default TrackList;